package ua.kiev.dyban.dao;


import ua.kiev.dyban.model.BrandAssociation;

import java.util.List;


/**
 * The interface Brand association dao.
 */
public interface BrandAssociationDao {

    /**
     * Find max price by type id brand association.
     *
     * @param typeId the type id
     * @return the brand association
     */
    BrandAssociation findMaxPriceByTypeId(Integer typeId);

    /**
     * Find all list.
     *
     * @return the list
     */
    List<BrandAssociation> findAll();

}
