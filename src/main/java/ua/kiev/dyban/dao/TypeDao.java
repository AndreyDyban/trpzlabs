package ua.kiev.dyban.dao;

import ua.kiev.dyban.model.Type;

import java.util.List;


/**
 * The interface Type dao.
 */
public interface TypeDao {

    /**
     * Find type.
     *
     * @param id the id
     * @return the type
     */
    Type find(Integer id);

    /**
     * Find all list.
     *
     * @return the list
     */
    List<Type> findAll();

}
