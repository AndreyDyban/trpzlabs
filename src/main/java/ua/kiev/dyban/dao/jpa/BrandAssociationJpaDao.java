package ua.kiev.dyban.dao.jpa;


import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.kiev.dyban.dao.BrandAssociationDao;
import ua.kiev.dyban.model.BrandAssociation;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.util.List;


/**
 * The type Brand association jpa dao.
 */
@Service("brandAssociationDao")
@Repository
@Transactional
public class BrandAssociationJpaDao implements BrandAssociationDao {

    @PersistenceContext(type = PersistenceContextType.EXTENDED)
    private EntityManager entityManager;

    @Override
    public BrandAssociation findMaxPriceByTypeId(Integer typeId) {
        return entityManager
                .createQuery("select c from BrandAssociation c where c.brand.price = (select max(e.brand.price) from BrandAssociation e where e.type.id = :typeId)", BrandAssociation.class)
                .setParameter("typeId", typeId)
                .getSingleResult();
    }

    @Override
    public List<BrandAssociation> findAll() {
        return entityManager.createQuery("SELECT c FROM BrandAssociation c").getResultList();
    }
}
