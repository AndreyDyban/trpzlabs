package ua.kiev.dyban.dao.jpa;


import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.kiev.dyban.dao.TypeDao;
import ua.kiev.dyban.model.Type;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;


/**
 * The type Type jpa dao.
 */
@Service("typeDao")
@Repository
@Transactional
public class TypeJpaDao implements TypeDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Type find(Integer id) {
        return entityManager.find(Type.class, id);
    }

    @Override
    public List<Type> findAll() {
        return entityManager.createQuery("SELECT c FROM Type c").getResultList();
    }

}
