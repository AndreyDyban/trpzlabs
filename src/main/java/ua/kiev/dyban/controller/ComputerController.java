package ua.kiev.dyban.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ua.kiev.dyban.dao.BrandAssociationDao;
import ua.kiev.dyban.dao.TypeDao;
import ua.kiev.dyban.model.BrandAssociation;

import java.util.List;

/**
 * Computer controller.
 */
@Controller
public class ComputerController {

    /**
     * The Brand association dao.
     */
    @Autowired
    public BrandAssociationDao brandAssociationDao;

    /**
     * The Type dao.
     */
    @Autowired
    public TypeDao typeDao;

    /**
     * Provide computer table.
     *
     * @param model  the model
     * @param typeId the type id
     * @return the string
     */
    @RequestMapping("/computers")
    public String provideComputerTable(Model model, @RequestParam(value="typeId", required=false) Integer typeId) {
        model.addAttribute ("brands", brandAssociationDao.findAll());
        model.addAttribute ("types", typeDao.findAll());
        return "computer/computers";
    }

}
