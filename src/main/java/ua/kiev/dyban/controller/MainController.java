package ua.kiev.dyban.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * Main controller.
 */
@Controller
public class MainController {

    /**
     * Provide the main page.
     *
     * @return the string
     */
    @RequestMapping("/")
    public String index(){
        return "index";
    }

}