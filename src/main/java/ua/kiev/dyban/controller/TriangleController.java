package ua.kiev.dyban.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ua.kiev.dyban.model.Triangle;
import ua.kiev.dyban.validation.TriangleFormValidator;


/**
 * Triangle controller.
 */
@Controller
@RequestMapping("/triangle")
public class TriangleController {

    /**
     * Provide triangle form.
     *
     * @param model the model
     * @return the string
     */
    @RequestMapping("/area")
    public String provideTriangleForm(Model model) {
        model.addAttribute ("triangle", new Triangle());
        return "triangle/areaForm";
    }

    /**
     * Process rectangle form string.
     *
     * @param triangle the triangle
     * @param result   the result
     * @param model    the model
     * @return the string
     */
    @RequestMapping(value = "/area", method = RequestMethod.POST)
    public String processRectangleForm(@ModelAttribute("triangle") Triangle triangle,
                                       BindingResult result, Model model) {
        new TriangleFormValidator().validate(triangle, result);
        if (result.hasErrors()) {
            return "triangle/areaForm";
        }
        model.addAttribute ("triangle", triangle);
        return "triangle/areaForm";
    }
}