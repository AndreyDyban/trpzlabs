package ua.kiev.dyban.model;

import javax.persistence.*;
import java.util.List;

/**
 * The type Type.
 */
@Entity
@Table(name="Types")
public class Type {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name="name", nullable = false, unique = true)
    private String name;

    @ManyToMany(mappedBy="types")
    private List<Brand> brands;

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets brands.
     *
     * @return the brands
     */
    public List<Brand> getBrands() {
        return brands;
    }

    /**
     * Sets brands.
     *
     * @param brands the brands
     */
    public void setBrands(List<Brand> brands) {
        this.brands = brands;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Type{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", brands=").append(brands);
        sb.append('}');
        return sb.toString();
    }
}
