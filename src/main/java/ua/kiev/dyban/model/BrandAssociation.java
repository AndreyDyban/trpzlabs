package ua.kiev.dyban.model;

import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The type Brand association.
 */
@Entity
@Table(name="BrandType")
public class BrandAssociation implements Serializable {

    @Id
    @Column(name = "brand_id")
    private int brandId;

    @Id
    @Column(name = "type_id")
    private int typeId;


    @ManyToOne
    @PrimaryKeyJoinColumn(name="brand_id", referencedColumnName="id")
    private Brand brand;

    @ManyToOne
    @PrimaryKeyJoinColumn(name="type_id", referencedColumnName="id")
    private Type type;

    /**
     * Gets brand id.
     *
     * @return the brand id
     */
    public int getBrandId() {
        return brandId;
    }

    /**
     * Sets brand id.
     *
     * @param brandId the brand id
     */
    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    /**
     * Gets type id.
     *
     * @return the type id
     */
    public int getTypeId() {
        return typeId;
    }

    /**
     * Sets type id.
     *
     * @param typeId the type id
     */
    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    /**
     * Gets brand.
     *
     * @return the brand
     */
    public Brand getBrand() {
        return brand;
    }

    /**
     * Sets brand.
     *
     * @param brand the brand
     */
    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public Type getType() {
        return type;
    }

    /**
     * Sets type.
     *
     * @param type the type
     */
    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BrandAssociation that = (BrandAssociation) o;

        if (brandId != that.brandId) return false;
        if (typeId != that.typeId) return false;
        if (brand != null ? !brand.equals(that.brand) : that.brand != null) return false;
        return type != null ? type.equals(that.type) : that.type == null;

    }

    @Override
    public int hashCode() {
        int result = brandId;
        result = 31 * result + typeId;
        result = 31 * result + (brand != null ? brand.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BrandAssociation{");
        sb.append("brandId=").append(brandId);
        sb.append(", typeId=").append(typeId);
        sb.append(", brand=").append(brand);
        sb.append(", type=").append(type);
        sb.append('}');
        return sb.toString();
    }
}