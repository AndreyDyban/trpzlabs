package ua.kiev.dyban.model;


/**
 * The type Triangle.
 */
public class Triangle {

    private double a;

    private double b;

    /**
     * Instantiates a new Triangle.
     */
    public Triangle() {
    }

    /**
     * Gets a.
     *
     * @return the a
     */
    public double getA() {
        return a;
    }

    /**
     * Gets b.
     *
     * @return the b
     */
    public double getB() {
        return b;
    }

    /**
     * Sets a.
     *
     * @param a the a
     */
    public void setA(double a) {
        this.a = a;
    }

    /**
     * Sets b.
     *
     * @param b the b
     */
    public void setB(double b) {
        this.b = b;
    }

    /**
     * Gets area.
     *
     * @return the area
     */
    public double getArea() {
        return (a * b) / 2;
    }
}
