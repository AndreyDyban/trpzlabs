package ua.kiev.dyban.model;

import javax.persistence.*;
import java.util.List;

/**
 * The type Brand.
 */
@Entity
@Table(name="Brands")
public class Brand {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name="name", nullable = false, unique = true)
    private String name;

    @Column(name="price", nullable = false)
    private String price;

    @ManyToMany
    @JoinTable(
            name="BrandType",
            joinColumns=@JoinColumn(name="brand_id", referencedColumnName="id"),
            inverseJoinColumns=@JoinColumn(name="type_id", referencedColumnName="id"))
    private List<Type> types;

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets price.
     *
     * @return the price
     */
    public String getPrice() {
        return price;
    }

    /**
     * Sets price.
     *
     * @param price the price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * Gets types.
     *
     * @return the types
     */
    public List<Type> getTypes() {
        return types;
    }

    /**
     * Sets types.
     *
     * @param types the types
     */
    public void setTypes(List<Type> types) {
        this.types = types;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Brand{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", price='").append(price).append('\'');
        sb.append(", types=").append(types);
        sb.append('}');
        return sb.toString();
    }
}
