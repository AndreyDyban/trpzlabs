package ua.kiev.dyban.validation;


import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ua.kiev.dyban.model.Triangle;

/**
 * The type Triangle form validator.
 */
public class TriangleFormValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return Triangle.class.equals(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Triangle triangle = (Triangle) target;

        if (triangle.getA() <= 0) {
            errors.rejectValue("a", "valid.triangle");
        }

        if (triangle.getB() <= 0) {
            errors.rejectValue("b", "valid.triangle");
        }

    }
}
