<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <title>TRPZ | Lab 1</title>

    <jsp:include page="/WEB-INF/views/fragments/head.jsp"/>

</head>

<body>

<jsp:include page="/WEB-INF/views/fragments/nav.jsp"/>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-md-3">
            <p class="lead">Лабораторні роботи ТРПЗ</p>
            <div class="list-group">
                <a href="/triangle/area" class="list-group-item active">Лаб 1</a>
                <a href="/computers" class="list-group-item">Лаб 2</a>
            </div>
        </div>

        <div class="col-md-9">

            <div class="page-header">
                <h1>Завдання :</h1>
            </div>
            <img src="/resources/img/lab1.png" class="img-responsive" alt="Lab 1">
            <br/>


            <form:form modelAttribute="triangle" action="/triangle/area"
                       method="post">

                <div class="form-group">
                    <form:label path="a">Side a :</form:label>
                    <form:input path="a" class="form-control"/>
                    <br/>
                    <form:errors path="a" cssClass="alert alert-danger" element="div"/>
                </div>

                <div class="form-group">
                    <form:label path="b">Side b :</form:label>
                    <form:input path="b" cssClass="form-control"/>
                    <br/>
                    <form:errors path="b" cssClass="alert alert-danger" element="div"/>
                </div>

                <form:button class="btn btn-default">Get area</form:button>
            </form:form>
            <br/>
            <c:if test="${triangle.area > 0.}">
                <div class="alert alert-success" role="alert">Area : ${triangle.area}</div>
            </c:if>

        </div>

    </div>

</div>
<!-- /.container -->

<jsp:include page="/WEB-INF/views/fragments/footer.jsp"/>

</body>

</html>