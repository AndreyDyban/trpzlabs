<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <title>TRPZ | Home</title>

    <jsp:include page="/WEB-INF/views/fragments/head.jsp"/>

</head>

<body>

<jsp:include page="/WEB-INF/views/fragments/nav.jsp"/>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-md-3">
            <p class="lead">Лабораторні роботи ТРПЗ</p>
            <div class="list-group">
                <a href="/triangle/area" class="list-group-item">Лаб 1</a>
                <a href="/computers" class="list-group-item">Лаб 2</a>
            </div>
        </div>

        <div class="col-md-9">



        </div>

    </div>

</div>
<!-- /.container -->

<jsp:include page="/WEB-INF/views/fragments/footer.jsp"/>

</body>

</html>