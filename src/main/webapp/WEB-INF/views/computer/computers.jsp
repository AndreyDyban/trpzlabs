<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <title>TRPZ | Lab 2</title>

    <jsp:include page="/WEB-INF/views/fragments/head.jsp"/>

</head>

<body>

<jsp:include page="/WEB-INF/views/fragments/nav.jsp"/>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-md-3">
            <p class="lead">Лабораторні роботи ТРПЗ</p>
            <div class="list-group">
                <a href="/triangle/area" class="list-group-item">Лаб 1</a>
                <a href="/computers" class="list-group-item active">Лаб 2</a>
            </div>
        </div>

        <div class="col-md-9">

            <div class="page-header">
                <h1>Завдання :</h1>
            </div>
            <img src="/resources/img/lab2.png" class="img-responsive" alt="Lab 2">
            <br/>

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Brand</th>
                    <th>Type</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${brands}" var="brandType">
                    <tr>
                        <td><c:out value="${brandType.brand.name}"/></td>
                        <td><c:out value="${brandType.type.name}"/></td>
                        <td><c:out value="${brandType.brand.price}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>


            <form id="data" action="/computers">
            </form>
            Most expensive :
            <select name="typeId" form="data" class="styled-select">
                <c:forEach items="${types}" var="type">
                    <c:choose>
                        <c:when test="${typeSelect != null && typeSelect.id == type.id}">
                            <option selected value="${type.id}">${type.name}</option>
                        </c:when>
                        <c:otherwise>
                            <option value="${type.id}">${type.name}</option>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </select>

            <input class=".btn-default" type="submit" form="data" value="Submit">

            <c:if test="${computer != null}">
                <div class="alert alert-success" role="alert">
                    Brand : ${computer.brand.name}<br/>
                    price : ${computer.brand.price}
                </div>
            </c:if>

        </div>

    </div>

</div>
<!-- /.container -->

<jsp:include page="/WEB-INF/views/fragments/footer.jsp"/>


</body>

</html>