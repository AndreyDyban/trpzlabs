CREATE TABLE IF NOT EXISTS `Brands` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `price` INT NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE IF NOT EXISTS `Types` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE IF NOT EXISTS `BrandType` (
  `brand_id` INT NOT NULL,
  `type_id` INT NOT NULL,
  PRIMARY KEY (`brand_id`, `type_id`),
  CONSTRAINT `brand_id_fk`
    FOREIGN KEY (`brand_id`)
    REFERENCES `Brands` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `type_id_fk`
    FOREIGN KEY (`type_id`)
    REFERENCES `Types` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

